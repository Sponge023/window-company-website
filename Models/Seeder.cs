﻿using BanatPlast.Data;
using BanatPlast.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

public class Seeder
{
    public static void SeedData(ApplicationDbContext context)
    {
        context.Database.EnsureCreated();

        var windows = new List<Window>();

        for (int i = 0; i < 9; i++)
        {
            var window = new Window
            {
                ImageUrl = $"wwwroot/images/window-images/window{i}.png",
                Height = 350,
                Width = 200,
                AdditionalInfo = ""
            };

            // Assign specific names and descriptions based on index i
            switch (i)
            {
                case 0:
                    window.Name = "Single Pane Classic";
                    window.Description = "A classic window design with a single pane, providing a timeless and elegant look. The simplicity of this window style allows for easy maintenance and cleaning. It is an excellent choice for traditional homes and those seeking a minimalist aesthetic. The single pane design allows ample natural light to illuminate the room, creating a warm and inviting atmosphere.";
                    break;
                case 1:
                    window.Name = "Double Pane Modern";
                    window.Description = "A modern window with double panes, offering enhanced insulation and contemporary style. The double pane construction provides improved energy efficiency by reducing heat transfer. This window style is popular for its sleek and clean lines, making it a perfect fit for modern and urban spaces. It also helps in noise reduction, creating a quiet and comfortable living environment. With its versatile design, the double pane modern window complements a variety of architectural styles.";
                    break;
                case 2:
                    window.Name = "Triple Pane Energy Saver";
                    window.Description = "An energy-efficient window featuring three panes for superior insulation and reduced energy consumption. The triple pane design provides exceptional thermal performance, keeping your home comfortable in all seasons. This window style is a smart choice for eco-conscious homeowners looking to minimize their environmental impact. In addition to energy savings, the triple pane window offers enhanced sound insulation, creating a quiet and peaceful indoor environment. Its advanced technology makes it a reliable and sustainable solution for modern living.";
                    break;
                case 3:
                    window.Name = "Side Hung Awning Window";
                    window.Description = "A side-hung awning window that opens outward, allowing for increased ventilation and a unique design. The side-hung mechanism provides easy operation and control over airflow. This window style is ideal for spaces where you want to capture refreshing breezes while maintaining privacy. The awning design also offers protection against light rain, making it a practical choice for various weather conditions. With its charming and functional features, the side-hung awning window adds character to both contemporary and traditional homes.";
                    break;
                case 4:
                    window.Name = "Sliding Glass Door";
                    window.Description = "A sliding glass door window, perfect for connecting indoor and outdoor spaces with a sleek and functional design. The sliding mechanism allows for effortless operation and seamless transitions between the interior and exterior. This window style is popular for its ability to maximize natural light and provide unobstructed views of the surroundings. It enhances the visual appeal of your living space while offering convenience and accessibility. Whether leading to a patio, garden, or balcony, the sliding glass door window creates a dynamic and open atmosphere in your home.";
                    break;
                case 5:
                    window.Name = "Casement Style Bay Window";
                    window.Description = "A bay window with casement style, providing a panoramic view and allowing for plenty of natural light. The casement design features outward-opening windows, allowing for excellent ventilation and easy cleaning. This window style creates a cozy alcove, perfect for reading or enjoying a scenic view. The bay window also adds architectural interest to the exterior of your home, enhancing curb appeal. With its versatility and timeless charm, the casement style bay window becomes a focal point in any room.";
                    break;
                case 6:
                    window.Name = "Arched Picture Window";
                    window.Description = "An arched picture window that adds architectural interest and a focal point to any room. The arched design adds a touch of elegance and sophistication to both traditional and contemporary interiors. This window style allows for expansive views and floods the room with natural light. Its distinctive shape creates a sense of openness and grandeur. The arched picture window serves as a beautiful backdrop, framing the outdoor scenery and creating a visually stunning element in your living space.";
                    break;
                case 7:
                    window.Name = "French Style Double Door";
                    window.Description = "A double door window in French style, adding charm and sophistication to your home. The French doors feature multiple glass panels, allowing for abundant natural light and a seamless connection between indoor and outdoor spaces. This window style embodies classic elegance and timeless beauty. The double doors create a wide opening, enhancing the flow between rooms and making them ideal for entertaining. With their graceful design and versatility, French style double doors elevate the aesthetics of any room.";
                    break;
                case 8:
                    window.Name = "Cottage Style Dormer Window";
                    window.Description = "A dormer window in charming cottage style, bringing character and uniqueness to your living space. The dormer design adds architectural interest to the roofline and creates a cozy alcove within the room. This window style is reminiscent of traditional cottage architecture, adding a touch of rustic charm. The dormer window allows natural light to fill the space, creating a warm and inviting atmosphere. With its quaint appeal and functional design, the cottage style dormer window becomes a delightful feature in any home.";
                    break;
            }

            windows.Add(window);
        }



        context.AddRange(windows);

        context.SaveChanges();
    }

}
