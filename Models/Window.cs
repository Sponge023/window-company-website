﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BanatPlast.Models
{
    public class Window
    {

        public int Id { get; set; }

        [Required]
        public string? Name { get; set; }

        public string? Description { get; set; }

        public string? ImageUrl { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        public string? AdditionalInfo { get; set; }
    }

}