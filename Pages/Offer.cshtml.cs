using BanatPlast.Data;
using BanatPlast.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BanatPlast.Pages
{
    public class OfferModel : PageModel
    {
        private readonly ApplicationDbContext _context;

        public OfferModel(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<Window>? WindowsList { get; set; }

        public void OnGet()
        {
            WindowsList = _context.Windows.ToList();
        }
    }
}
