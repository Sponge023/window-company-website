using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace BanatPlast.Pages
{
    public class GalleryModel : PageModel
    {

        public GalleryModelDTO ModelDTO { get; private set; }


        public void OnGet()
        {
            // Replace "wwwroot/images/" with your actual folder path
            var folderPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images/gallery-pics");
            ModelDTO = new GalleryModelDTO
            {
                GalleryImages = Directory.GetFiles(folderPath)
                                    .Where(file => file.ToLower().EndsWith(".jpg") || file.ToLower().EndsWith(".png"))
                                    .Select(Path.GetFileName)
                                    .ToList()
            };
        }
    }


    public class GalleryModelDTO
    {
        public List<string> GalleryImages { get; set; }
    }
}
