﻿using BanatPlast.Data;
using BanatPlast.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

[Route("api/windows")]
[ApiController]
public class WindowsController : ControllerBase
{
    private readonly ApplicationDbContext _context;

    public WindowsController(ApplicationDbContext context)
    {
        _context = context;
    }


    [HttpGet("{id}")]
    public async Task<IActionResult> GetWindowDetails(int id)
    {
        var window = await _context.Windows.FindAsync(id);

        if (window == null)
        {
            return NotFound();
        }

        return Ok(window);
    }

}
