﻿const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const cors = require('cors'); // Import the cors middleware


const app = express();
const port = process.env.PORT || 3000;

app.use(cors()); // Enable CORS for all routes
app.use(bodyParser.json());

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'GMAIL@GMAIL.COM', // Replace with your Gmail address
        pass: 'PASSWORD123' // Replace with your App Password
    }
});

app.post('/send-email', async (req, res) => {
    try {
        const { name,email,phone, message } = req.body;

        const mailOptions = {
            from: email,
            to: 'your@gmail.com', // Replace with your Gmail address
            subject: `[BANATPLAST] Novi mail od korisnika ${name}`, // You can include the user's name here if it's sent from a form field
            text: `Email: ${email}\nMessage: ${message}`
        };

        await transporter.sendMail(mailOptions);

        res.status(200).send('Email sent successfully!');
    } catch (error) {
        console.error('Error sending email:', error);
        res.status(500).send('Error sending email');
    }
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
