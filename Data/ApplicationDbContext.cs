﻿using BanatPlast.Models;
using Microsoft.EntityFrameworkCore;

namespace BanatPlast.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Window> Windows { get; set; }




        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Window>(entity =>
            {
                entity.HasKey(e => e.Id); // Assuming Id is the primary key

                // Configure other properties as needed
                entity.Property(e => e.Name).IsRequired();
                entity.Property(e => e.Description).IsRequired(false); // Assuming Description is optional
                entity.Property(e => e.ImageUrl).IsRequired(false); // Assuming ImageUrl is optional
                entity.Property(e => e.Height).IsRequired();
                entity.Property(e => e.Width).IsRequired();
                entity.Property(e => e.AdditionalInfo).IsRequired(false); // Assuming AdditionalInfo is optional
            });

            base.OnModelCreating(modelBuilder);
        }


    }
}